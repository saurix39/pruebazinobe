# Challenge Python L1
En este repositorio se encuentra la prueba técnica para Zinobe.
En la carpeta con nombre **"Prueba"** se encuentra la aplicación, en la cual se ha hecho la creación de clases para realizar las peticiones y la conexión con la base de datos. Dentro de las modificaciones realizadas se encuentran la creación de un apartado presentación en donde se ubica la clase window, esta representa la interfaz gráfica en donde se puede visualizar la información obtenida. Se ha añadido un script llamado **`test.py`** en donde se encuentran todas las pruebas unitarias realizadas a los diferentes métodos y funciones del software. **Se ha actualizado el endpoint** del api al cual se estaba haciendo la petición GET para obtener la información.

## Aspectos a tener en cuenta
1. **Los cambios principales se han hecho en la carpeta prueba.**
2. Para su desarrollo se ha utilizado la versión 3.9.4 de Python.
3. En el documento requirements.txt se encuentran las dependencias necesarias para el funcionamiento de la aplicación.
4. Preferiblemente usar un entorno virtual.
5. Al haberse actualizado el endpoint y utilizar únicamente rest countries como api para extraer la información ya no se hace una petición por cada región a el api rapidapi, lo cual agiliza el proceso y de esta manera el tiempo que tarda en generar cada fila es minimo; es por esta razón que, el tiempo que normalmente se ve en la columna Time es el de la penalización referente a la petición inicial de toda la información.
6. Los test unitarios están hechos de forma manual debido a que uno de los requerimientos de esta prueba es no usar frameworks, de lo contrario hubiese utilizado unittest.

## Diseño de la solución
<img src="./Prueba/solutionDesign.jpg" alt="Imagen de la solución">

