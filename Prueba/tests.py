import index
from Logica.peticion import Peticion
from Persistencia import conexion
import pandas as pd

class TestingApp():
    def testEncriptarIdio(self):
        case1 = index.encriptarIdio("English") == "649DF08A448EE3FA90F3746BAAF6B0907DF42C91"
        case2 = index.encriptarIdio("Spanish") == "8DF7F1B361B2AF42D36011E00D22C0F9891EC0B0"
        case3 = index.encriptarIdio("TestWord") == "A5825EC84C36F5AA5B8A0732C05E333A2EF8C1FA"
        return case1 and case2 and case3
    
    def testGetLanguage(self):
        case1 = index.getLanguage({"eng":"English"}) == "English"
        case2 = index.getLanguage({"msa": "Malay","eng":"English"}) == "Malay"
        case3 = index.getLanguage({"rus": "Russian","tgk": "Tajik","nfr": "Guernésiais"}) == "Russian"
        return case1 and case2 and case3

    def testGetPenaltyValue(self):
        case1 = index.getPenaltyValue(1634745276.7500794,8,1634745278.7528236) == 0.2503430247306824
        case2 = index.getPenaltyValue(1634745700.556751,6,1634745702.5654535) == 0.33478375275929767
        case3 = index.getPenaltyValue(1634747291.551891,10,1634747295.5605726) == 0.4008681535720825
        return case1 and case2 and case3

    def testGetData(self):
        peticion = Peticion("https://restcountries.com/v3.1/")
        expected = {
            "name": {
                "common": "Colombia",
                "official": "Republic of Colombia",
                "nativeName": {
                    "spa": {
                        "official": "República de Colombia",
                        "common": "Colombia"
                    }
                }
            },
            "region": "Americas",
            "languages": {
                "spa": "Spanish"
            }
        }
        case1 = peticion.getData("name","name,region,languages","Colombia")[0] == expected
        return case1
    
    def testFilterInfo(self):
        peticion = Peticion("https://restcountries.com/v3.1/")
        input =[
            {
                "name": {
                    "common": "Malaysia",
                    "official": "Malaysia",
                    "nativeName": {
                        "eng": {
                            "official": "Malaysia",
                            "common": "Malaysia"
                        },
                        "msa": {
                            "official": "مليسيا",
                            "common": "مليسيا"
                        }
                    }
                },
                "region": "Asia",
                "languages": {
                    "eng": "English",
                    "msa": "Malay"
                }
            },
            {
                "name": {
                    "common": "Tajikistan",
                    "official": "Republic of Tajikistan",
                    "nativeName": {
                        "rus": {
                            "official": "Республика Таджикистан",
                            "common": "Таджикистан"
                        },
                        "tgk": {
                            "official": "Ҷумҳурии Тоҷикистон",
                            "common": "Тоҷикистон"
                        }
                    }
                },
                "region": "Asia",
                "languages": {
                    "rus": "Russian",
                    "tgk": "Tajik"
                }
            }
        ]
        expected=[
            {
                "region":"Asia",
                "name":"Malaysia",
                "language":{
                    "eng": "English",
                    "msa": "Malay"
                }
            }
        ]
        case1 = peticion.filterInfo(input) == expected
        return case1
    
    def testBuildDataFrame(self):
        data = pd.DataFrame(columns=['Region','City Name','Language','Time'])
        infoInput = [
            {
                "region":"Oceania",
                "name":"American Samoa",
                "language":{
                    "eng": "English",
                    "smo": "Samoan"
                }
            },
            {
                "region":"Europe",
                "name":"Italy",
                "language":{
                    "ita": "Italian"
                }
            },
            {
                "region":"Americas",
                "name":"Mexico",
                "language":{
                    "spa": "Spanish"
                }
            }
        ]
        regions = ["Oceania","Europe","Americas"]
        countries = ["American Samoa","Italy","Mexico"]
        languages = ["649DF08A448EE3FA90F3746BAAF6B0907DF42C91", "485B57ADCC80650A0F376FE516E0EA35FEF68007","8DF7F1B361B2AF42D36011E00D22C0F9891EC0B0"]
        expectedDataFrame = pd.DataFrame()
        expectedDataFrame["Region"]=regions
        expectedDataFrame["City Name"]=countries
        expectedDataFrame["Language"]=languages
        result = index.buildDataFrame(data,0.02,infoInput).drop(['Time'],axis=1)
        return not(False in (result == expectedDataFrame).values)

    def testDatabaseConnection(self):
        try:
            con = conexion.conexion()
            test = con.testingConnection()
        except:
            case1 = False
        con.cerrarCon()
        case1 = test == 2
        return case1


if __name__ == "__main__":
    tests=[]
    test = TestingApp()
    tests.append(test.testEncriptarIdio())
    tests.append(test.testGetLanguage())
    tests.append(test.testGetPenaltyValue())
    tests.append(test.testGetData())
    tests.append(test.testFilterInfo())
    tests.append(test.testBuildDataFrame())
    tests.append(test.testDatabaseConnection())
    flag = True
    for test in tests:
        if not test:
            flag = False
            break
    if(flag):
        print("Ran "+str(len(tests))+" tests")
        print("OK")
    else:
        print("Failed unit testings")
        print("Failed:")
        [print("Test"+str(idx+1)) if test==False else None for idx,test in enumerate(tests)]

        

