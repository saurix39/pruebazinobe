
import hashlib
import pandas as pd
from time import time
from Logica.peticion import Peticion
from Persistencia import conexion
from Presentacion import window

def encriptarIdio(idioma):
    idioEnc=hashlib.sha1(idioma.encode('utf-8'))
    return idioEnc.hexdigest().swapcase()

def getPenaltyValue(start,total,end):
    return (end-start)/total

def getLanguage(lang):
    return list(lang.values())[0]

def buildDataFrame(dataFrame, penalty, data):
    for dataTable in data:
        start = time()
        idioEnc=encriptarIdio(getLanguage(dataTable["language"]))
        datos=pd.Series(
            [dataTable["region"],
            dataTable["name"],
            idioEnc,
            round((time()-start)+penalty,2)],
            index=['Region','City Name','Language','Time']
        )
        dataFrame=dataFrame.append(datos,ignore_index=True)
    return dataFrame

if __name__== "__main__":
    start_1 = time()
    peticion = Peticion("https://restcountries.com/v3.1/")
    allInfo = peticion.getData("all" ,"name,region,languages")
    info = peticion.filterInfo(allInfo)
    data = pd.DataFrame(columns=['Region','City Name','Language','Time'])
    penalty = getPenaltyValue(start_1,len(info),time())
    data = buildDataFrame(data,penalty,info)
    print(data)
    print("Tiempo total:    "+str(data['Time'].sum()))
    print("Tiempo promedio: "+str(data['Time'].mean()))
    print("Tiempo minimo:   "+str(data['Time'].min()))
    print("Tiempo maximo:   "+str(data['Time'].max()))
    con = conexion.conexion()
    con.almacenar(data)
    con.cerrarCon()
    data.to_json("data.json",orient="index")
    window = window.Window(data,str(data['Time'].sum()),str(data['Time'].mean()),str(data['Time'].min()),str(data['Time'].max()))
    window.getApp().mainloop()
