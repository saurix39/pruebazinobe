from sqlite3 import connect

class conexion:
    def __init__(self):
        self.con=connect("base.db")
    def almacenar(self, data):
        data.to_sql('data', self.con, if_exists="replace")
    def cerrarCon(self):
        self.con.close()
    def testingConnection(self):
        cur = self.con.cursor()
        return list(cur.execute("select 1+1"))[0][0]