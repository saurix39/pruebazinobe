from tkinter import ttk
from tkinter import Tk
from tkinter import *

class Window:
    def __init__(self, data, suma, mean, mini, maxi):
        self.data = data
        self.sum = suma
        self.mean = mean
        self.min = mini
        self.max = maxi
        self.app = Tk()
        self.app.title("Visualización de información")
        self.app.geometry("780x350")
        self.tabla = ttk.Treeview(self.app, height=10, columns=("Region","City name","Languaje","Time"))
        self.tabla.column("#0", width=50)
        self.tabla.column("Region", width=150)
        self.tabla.column("City name", width=200)
        self.tabla.column("Languaje", width=300)
        self.tabla.column("Time", width=50)
        self.tabla.heading("#0", text='', anchor=CENTER)
        self.tabla.heading("Region", text='Region', anchor=CENTER)
        self.tabla.heading("City name", text='City name', anchor=CENTER)
        self.tabla.heading("Languaje", text='Languaje', anchor=CENTER)
        self.tabla.heading("Time", text='Time', anchor=CENTER)
        self.lbltotal = Label(self.app, text="Tiempo total: ")
        self.lblprome = Label(self.app, text="Tiempo promedio: ")
        self.lblmini = Label(self.app, text="Tiempo minimo: ")
        self.lblmaxi = Label(self.app, text="Tiempo maximo: ")
        self.total = Label(self.app, text=self.sum)
        self.prome = Label(self.app, text=self.mean)
        self.mini = Label(self.app, text=self.min)
        self.maxi = Label(self.app, text=self.max)
        self.lbltotal.place(x=15,y=250)
        self.lblprome.place(x=15,y=270)
        self.lblmini.place(x=15,y=290)
        self.lblmaxi.place(x=15,y=310)
        self.total.place(x=125,y=250)
        self.prome.place(x=125,y=270)
        self.mini.place(x=125,y=290)
        self.maxi.place(x=125,y=310)
        self.tabla.place(x=15,y=5)
        self.procData()

    def procData(self):
        df_rows=self.data.to_numpy().tolist()
        i=len(df_rows)-1
        for row in reversed(df_rows):
            self.tabla.insert('',0,text=i,values=(row[0],row[1],row[2],row[3]))
            i=i-1

    def getApp(self):
        return self.app

