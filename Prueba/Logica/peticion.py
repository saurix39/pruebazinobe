import requests
import numpy as np

class Peticion:
    def __init__(self, url):
        self.url = url
    def getData(self, tipe ,fields, extra=None):
        url = self.url + tipe +"?fields="+fields if tipe == "all" else self.url+"/" + tipe +"/"+ extra+"?fields="+fields
        response = requests.request("GET", url).json()
        return response
    def filterInfo(self,data):
        dataRegion=[]
        regions = np.array([country["region"] for country in data])
        regions = np.unique(regions,True)
        [dataRegion.append(
            {
                "region":data[index]["region"],
                "name":data[index]["name"]["common"],
                "language":data[index]["languages"]
            }
        ) for index in regions[1]]
        return dataRegion